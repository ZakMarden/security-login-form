# Login form application

This is a small Python + Flask web application implementing a signup page and a login page. 

## How to run

```bash
# Install packages
pip install -r requirements.txt

# Run the app
export FLASK_APP=login_form

flask init-db
flask run --host=0.0.0.0
```

You should now be able to browse the app at `http://localhost:5000`


<!-- BEGIN GENERATED SECTION DO NOT EDIT -->

---

**How was this resource?**  
[😫](https://airtable.com/shrUJ3t7KLMqVRFKR?prefill_Repository=makers-students%2Fdevops-course&prefill_File=security%2Fsolo_project%2Flogin-form%2FREADME.md&prefill_Sentiment=😫) [😕](https://airtable.com/shrUJ3t7KLMqVRFKR?prefill_Repository=makers-students%2Fdevops-course&prefill_File=security%2Fsolo_project%2Flogin-form%2FREADME.md&prefill_Sentiment=😕) [😐](https://airtable.com/shrUJ3t7KLMqVRFKR?prefill_Repository=makers-students%2Fdevops-course&prefill_File=security%2Fsolo_project%2Flogin-form%2FREADME.md&prefill_Sentiment=😐) [🙂](https://airtable.com/shrUJ3t7KLMqVRFKR?prefill_Repository=makers-students%2Fdevops-course&prefill_File=security%2Fsolo_project%2Flogin-form%2FREADME.md&prefill_Sentiment=🙂) [😀](https://airtable.com/shrUJ3t7KLMqVRFKR?prefill_Repository=makers-students%2Fdevops-course&prefill_File=security%2Fsolo_project%2Flogin-form%2FREADME.md&prefill_Sentiment=😀)  
Click an emoji to tell us.

<!-- END GENERATED SECTION DO NOT EDIT -->
